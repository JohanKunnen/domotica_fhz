# Domotica FHZ

This is a python package that can be used for FHZ 1000/FHZ 1300 and FS20 devices.  

More information about these devices can be found [here](http://fhz4linux.info/tiki-index.php).
